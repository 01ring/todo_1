# ToDo example - ReactJS

* Setting up React for ES6 with Webpack and Babel: https://www.twilio.com/blog/2015/08/setting-up-react-for-es6-with-webpack-and-babel-2.html
* Getting Started with Redux: https://egghead.io/series/getting-started-with-redux

## Launch command
`webpack-dev-server --progress --colors`
