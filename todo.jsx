import React from 'react';
import ReactDOM from 'react-dom';
// import Redux from 'react-redux';
import { combineReducers } from 'redux';
import { createStore } from 'redux';
import { connect, Provider } from 'react-redux'

const todo = (state, action) => {
  switch (action.type) {
    case 'ADD_TODO':
      return {
        id: action.id,
        text: action.text,
        completed: false
      };
    case 'TOGGLE_TODO':
      if (state.id !== action.id) {
        return state;
      }
      
      return Object.assign({}, state, {completed: !state.completed});
    default:
      return state;
  }
};

const todos = (state = [], action) => {
  switch (action.type) {
    case 'ADD_TODO':
      var stateCopy = state.slice();
      stateCopy.push(todo(undefined, action));
      return stateCopy;
    case 'TOGGLE_TODO':
      return state.map(t => todo(t, action));
    default:
      return state;
  }
};

const visibilityFilter = (
  state = 'SHOW_ALL',
  action
) => {
  switch (action.type) {
    case 'SET_VISIBILITY_FILTER':
      return action.filter;
    default:
      return state;
  }
};

const todoApp = combineReducers({todos, visibilityFilter});
const store = createStore(todoApp);

let nextTodoId = 0;
class Todo extends React.Component {
  render() {
    return <div>
      <input ref={node => {this.input = node}} />
      <button onClick={() => {
        this.props.dispatch({
          type: 'ADD_TODO',
          text: this.input.value,
          id: nextTodoId++
        });
        this.input.value = '';
      }}>
        Add ToDo
      </button>
      <ul>
        {
          this.props.todos.map((todo) => {
            return <li key={todo.id}>{todo.text}</li>;
          })
        }
      </ul>
    </div>
  }
}


const TodoApp = connect(state => state)(Todo);
ReactDOM.render(
  <Provider store={store}>
    <TodoApp />
  </Provider>,
  document.getElementById('root')
)
